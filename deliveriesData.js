const csvToJson = require("convert-csv-to-json");

const deliveriesObject = csvToJson.fieldDelimiter(',').getJsonFromCsv("./data/deliveries.csv");
module.exports = deliveriesObject;
