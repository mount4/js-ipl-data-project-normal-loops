
const matchesObject = require("./matchesData");

function matchesWonPerTeam(matchObject) {
    const result = {};
    for (let index = 0; index < matchObject.length; index++) {
        const match = matchObject[index];
        const winnerteam = match.winner;
        if (result[winnerteam] == null) {
            result[winnerteam] = 1;
        } else {
            result[winnerteam] += 1;
        }
    }

    console.log(JSON.stringify(result));
    return JSON.stringify(result);
}

matchesWonPerTeam(matchesObject);
// module.exports = matchesPlayedPerYear;
