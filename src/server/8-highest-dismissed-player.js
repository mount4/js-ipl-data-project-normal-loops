const deliveriesObject = require("../../deliveriesData");
const saveJsonToFile = require("./save-output");

function highestDismissedByPlayer(dataObject) {
    let result = [];
    let maxDismissed = 0;

    for (bowler in dataObject) {
        const dismissed = dataObject[bowler];

        if (dismissed > maxDismissed) {
            const bowlerObject = {
                name: bowler,
                dismissed: dismissed,
            };
            result[0] = bowlerObject;
            maxDismissed = dismissed;
        } else if (dismissed == maxDismissed) {
            const bowlerObject = {
                name: bowler,
                dismissed: dismissed,
            };
            result.push(bowlerObject);
        }
    }
    return result;
}

function highestDismissedPlayer(dataObject, player) {
    
    // const result = dataObject.reduce((accumalator, deliveryObject) => {
    //     const playerDismissed = deliveryObject.player_dismissed;

    //     if (player == playerDismissed && playerDismissed != null) {
    //         const bowler = deliveryObject.bowler;
    //         if (accumalator[bowler] == null) {
    //             accumalator[bowler] = 1;
    //         } else {
    //             accumalator[bowler] += 1;
    //         }
    //     }

    //     return accumalator;
    // }, {});
    const result = {};
    for (let index = 0; index < dataObject.length; index++) {
        const deliveryObject = dataObject[index];
        const playerDismissed = deliveryObject.player_dismissed;

        if (player == playerDismissed && playerDismissed != null) {
            const bowler = deliveryObject.bowler;
            if (result[bowler] == null) {
                result[bowler] = 1;
            } else {
                result[bowler] += 1;
            }
        }
    }

    const highestDismissedPlayerData = highestDismissedByPlayer(result);
    console.log(JSON.stringify(highestDismissedPlayerData));
    return JSON.stringify(highestDismissedPlayerData);
}

const jsonResult = highestDismissedPlayer(deliveriesObject, "DA Warner");
// saveJsonToFile(jsonResult, "highest-dismissed-by-player", "./output");
