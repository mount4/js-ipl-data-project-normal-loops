const matchesObject = require("../../matchesData");
const saveJsonToFile = require("./save-output");

function manOfTheMathes(matchObject) {
    const result = {};

    for (let index = 0; index < matchObject.length; index++) {
        const match = matchObject[index];
        const year = match.season;
        const manOfTheMatch = match.player_of_match;

        if (result[year] == null) {
            result[year] = {};
        }

        if (result[year][manOfTheMatch] == null) {
            result[year][manOfTheMatch] = 1;
        } else {
            result[year][manOfTheMatch] += 1;
        }
    }
    return result;
}

function topManOfTheMatches(matchObject) {
    const dataObj = manOfTheMathes(matchObject);
    const result = {};
    for (year in dataObj) {
        const yearPlayer = dataObj[year];
        const bestPlayer = {
            name: "",
            awards: 0,
        };

        for (player in yearPlayer) {
            const award = yearPlayer[player];
            if (award > bestPlayer.awards) {
                bestPlayer.name = player;
                bestPlayer.awards = award;
            }
        }
        result[year] = bestPlayer;
    }
    console.log(JSON.stringify(result));
    return JSON.stringify(result);
}

const jsonResult = topManOfTheMatches(matchesObject);

saveJsonToFile(jsonResult, "highest-man-of-the-match-season", "./output");
