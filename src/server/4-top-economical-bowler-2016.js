const matchesObject = require("../../matchesData");
const deliveriesObject = require("../../deliveriesData");
const getMatchId = require("../../getMatchId");
const saveJsonToFile = require("./save-output");

function calculateEconomy(result) {
    let newResult = [];
    for (let i = 0; i < result.length; i++) {
        let bowlerObj = result[i];
        let overs = bowlerObj.balls / 6;
        let economy = (bowlerObj.runs / overs).toFixed(2);
        newResult.push({ name: bowlerObj.name, economy: economy });
    }
    return newResult;
}


function getEconomyRate(data) {
    let result = Object.values(data);
    result = calculateEconomy(result)

    const sortedResult = result.sort((a, b) => {
        return a.economy - b.economy;
    });
    
    return sortedResult.slice(0, 10);
}

function topEconomicalBowler(matchesData, deliveriesData) {
    const result = {};
    const match2016Id = getMatchId(matchesData, 2016);

    for (let index = 0; index < deliveriesData.length; index++) {
        const delivery = deliveriesData[index];
        const matchId = delivery.match_id;

        if (match2016Id.includes(matchId)) {
            const bowler = delivery.bowler;
            const totalRuns = Number(delivery.total_runs);
            if (result[bowler] == null) {
                result[bowler] = {};
                result[bowler].runs = totalRuns;
                result[bowler].balls = 1;
                result[bowler].name = bowler;
            } else {
                result[bowler].runs += totalRuns;
                result[bowler].balls += 1;
            }
        }
    }

    const economyResult = getEconomyRate(result);
    console.log(JSON.stringify(economyResult));
    return JSON.stringify(economyResult);
}

 
const jsonResult = topEconomicalBowler(matchesObject, deliveriesObject);

saveJsonToFile(jsonResult, 'top-economical-bowler-2016', './output');