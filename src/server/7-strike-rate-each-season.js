const deliveriesObject = require("../../deliveriesData");
const getEachYearMatchId = require("../../getEachMatchYearId");
const matchesObject = require("../../matchesData");
const saveJsonToFile = require("./save-output");

function strikeRateEachSeasonPlayer(dataObject, player) {
    const result = {};
    for (year in dataObject) {
        if (player in dataObject[year]) {
            const playerData = dataObject[year][player];
            const totalRuns = playerData[0];
            const totalBalls = playerData[1];
            const strikeRate = String(
                ((totalRuns / totalBalls) * 100).toFixed(2) + " %"
            );
            result[year] = strikeRate;
        } else {
            result[year] = "notPlayed";
        }
    }
    console.log(JSON.stringify(result));
    return JSON.stringify(result);
}

function strikeRateEachSeason(dataObject, matchObject) {
    const matchYear = getEachYearMatchId(matchObject);
    
    // const result = dataObject.reduce((accumalator, deliveryObject) => {
    //     const batsman = deliveryObject.batsman;
    //     const batsmanRun = deliveryObject.batsman_runs;
    //     const matchId = deliveryObject.match_id;
    //     const year = matchYear[matchId];
    //     if (accumalator.hasOwnProperty(year) == false) {
    //         accumalator[year] = {};
    //     }
    //     if (accumalator[year][batsman] == null) {
    //         accumalator[year][batsman] = [Number(batsmanRun), 1];
    //     } else {
    //         accumalator[year][batsman][0] += Number(batsmanRun);
    //         accumalator[year][batsman][1] += 1;
    //     }
    //     return accumalator;
    // }, {});

    const batsmanResult = {};
    for(let index = 0; index <dataObject.length; index++){
        const deliveryObject = dataObject[index];
        const batsman = deliveryObject.batsman;
        const batsmanRun = deliveryObject.batsman_runs;
        const matchId = deliveryObject.match_id;
        const year = matchYear[matchId];
        if (!batsmanResult.hasOwnProperty(year)) {
            batsmanResult[year] = {};
        }
        if (!batsmanResult[year].hasOwnProperty(batsman)) {
            batsmanResult[year][batsman] = [Number(batsmanRun), 1];
        } else {
            batsmanResult[year][batsman][0] += Number(batsmanRun);
            batsmanResult[year][batsman][1] += 1;
        }
    }
    return batsmanResult;
}

const strikeRateEachSeasonData = strikeRateEachSeason(
    deliveriesObject,
    matchesObject
);
const playerName = "RV Uthappa";


const jsonResult = strikeRateEachSeasonPlayer(strikeRateEachSeasonData, playerName);

// saveJsonToFile(jsonResult, 'strike-rate-each-season', './output');
