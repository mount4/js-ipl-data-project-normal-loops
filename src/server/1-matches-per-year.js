const matchesObject = require("../../matchesData");
const saveJsonToFile = require("./save-output");

function matchesPlayedPerYear(matchObject) {
    const result = {};
    for (let index = 0; index < matchObject.length; index++) {
        match = matchObject[index];
        year = match.season;
        if (result[year] == null) {
            result[year] = 1;
        } else {
            result[year] += 1;
        }
    }

    console.log(JSON.stringify(result));
    return JSON.stringify(result);
}

const jsonResult = matchesPlayedPerYear(matchesObject);

saveJsonToFile(jsonResult, 'matches-per-year', './output');
// module.exports = matchesPlayedPerYear;
