const matchesObject = require("../../matchesData");
const deliveriesObject = require("../../deliveriesData");
const getMatchId = require("../../getMatchId");
const saveJsonToFile = require("./save-output");

function extra_run_conceded_team(matchesData, deliveriesData) {
    const result = {};
    const match2016Id = getMatchId(matchesData, 2016);

    for (let index = 0; index < deliveriesData.length; index++) {
        const delivery = deliveriesData[index];
        const matchId = delivery.match_id;

        if (match2016Id.includes(matchId)) {
            const bowlingTeam = delivery.bowling_team;
            const extraRuns = Number(delivery.extra_runs);
            if (result[bowlingTeam] == null) {
                result[bowlingTeam] = extraRuns;
            } else {
                result[bowlingTeam] += extraRuns;
            }
        }
    }

    console.log(JSON.stringify(result));
    return JSON.stringify(result);
}

const jsonResult = extra_run_conceded_team(matchesObject, deliveriesObject);

saveJsonToFile(jsonResult, "extra-runs-conceded-per-team", "./output");
