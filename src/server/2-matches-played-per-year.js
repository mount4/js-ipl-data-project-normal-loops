const matchesObject = require("../../matchesData");
const saveJsonToFile = require("./save-output");

function matchesWonPerTeam(matcheObject) {
    const result = {};
    for (let index = 0; index < matcheObject.length; index++) {
        const match = matcheObject[index];
        const winnerteam = match.winner;
        const year = match.season;
        if(result.hasOwnProperty(year) == false){
            result[year] = {}
        }
        if (result[year][winnerteam] == null) {
            result[year][winnerteam] = 1;
        } else {
            result[year][winnerteam] += 1;
        }
    }

    console.log(JSON.stringify(result));
    return JSON.stringify(result);
}

const jsonResult = matchesWonPerTeam(matchesObject);

saveJsonToFile(jsonResult, "matches-won-per-team", "./output");

// module.exports = matchesPlayedPerYear;
