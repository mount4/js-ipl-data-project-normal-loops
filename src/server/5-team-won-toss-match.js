const matchesObject = require("../../matchesData");
const saveJsonToFile = require("./save-output");

function matchesWonTossWonTeam(matcheObject) {
    const result = {};
    for (let index = 0; index < matcheObject.length; index++) {
        const match = matcheObject[index];
        const winnerteam = match.winner;
        const winnerToss = match.toss_winner;
        if (winnerToss == winnerteam) {
            if (result[winnerteam] == null) {
                result[winnerteam] = 1;
            } else {
                result[winnerteam] += 1;
            }
        }
    }

    console.log(JSON.stringify(result));
    return JSON.stringify(result);
}

const jsonResult = matchesWonTossWonTeam(matchesObject);

saveJsonToFile(jsonResult, "team-won-toss-match", "./output");
// module.exports = matchesPlayedPerYear;
