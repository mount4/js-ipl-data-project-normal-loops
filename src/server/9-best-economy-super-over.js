const deliveriesObject = require("../../deliveriesData");
const saveJsonToFile = require("./save-output");

function economySuperOver(dataObject) {
    // const result = dataObject.reduce((accumalator, deliveryObject) => {
    //     const superOver = deliveryObject.is_super_over;
    //     if (superOver == 1) {
    //         const bowler = deliveryObject.bowler;
    //         const runs = Number(deliveryObject.total_runs);

    //         if (accumalator[bowler] == null) {
    //             accumalator[bowler] = [runs, 1];
    //         } else {
    //             accumalator[bowler][0] += runs;
    //             accumalator[bowler][1] += 1;
    //         }
    //     }
    //     return accumalator;
    // }, {});

    const result = {};
    for (let i = 0; i < dataObject.length; i++) {
        const deliveryObject = dataObject[i];
        const superOver = deliveryObject.is_super_over;
        if (superOver == 1) {
            const bowler = deliveryObject.bowler;
            const runs = Number(deliveryObject.total_runs);

            if (!result.hasOwnProperty(bowler)) {
                result[bowler] = [runs, 1];
            } else {
                result[bowler][0] += runs;
                result[bowler][1] += 1;
            }
        }
    }
    
    for (player in result) {
        const runs = result[player][0];
        const balls = result[player][1];
        const overs = Math.ceil(balls / 6);
        result[player] = parseFloat((runs / overs).toFixed(2));
    }
    return result;
}

function bestEconomyRate(dataObject) {
    const bowlerEconomySuperOver = economySuperOver(dataObject);
    let bestEconomy = 99;
    const result = {};
    for (player in bowlerEconomySuperOver) {
        const playerEconomy = bowlerEconomySuperOver[player];
        if (playerEconomy < bestEconomy) {
            result.bowler = player;
            result.economy = playerEconomy;
            bestEconomy = playerEconomy;
        }
    }
    console.log(JSON.stringify(result));
    return JSON.stringify(result);
}

const jsonResult = bestEconomyRate(deliveriesObject);
// saveJsonToFile(jsonResult, "best-economy-super-over", "./output");
