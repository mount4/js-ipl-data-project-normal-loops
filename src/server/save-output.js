const fs = require('fs');
const path = require('path');

function saveJsonToFile(jsonData, fileName, outputDir = './save') {
    if (!fs.existsSync(outputDir)) {
        fs.mkdirSync(outputDir, { recursive: true });
    }

    const outputFilePath = path.join(outputDir, fileName+'.json');

    try {
        fs.writeFileSync(outputFilePath, jsonData);
        console.log(`JSON data saved to ${outputFilePath}`);
    } catch (error) {
        console.error('Error saving JSON data:', error);
    }
}


module.exports = saveJsonToFile;