const csvToJson = require("convert-csv-to-json");

const matchesObject = csvToJson.fieldDelimiter(',').getJsonFromCsv("./data/matches.csv");
module.exports = matchesObject;
