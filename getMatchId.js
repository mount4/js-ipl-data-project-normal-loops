function getMatchId(dataObject, year) {
    const result = [];
    for (let index = 0; index < dataObject.length; index++) {
        const match = dataObject[index];
        const matchYear = match.season;
        const id = match.id;
        if (matchYear == year) {
            result.push(id);
        }
    }
    console.log(result);
    return result;
}

module.exports = getMatchId;