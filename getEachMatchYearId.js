function getEachYearMatchId(dataObject) {
    const result = {};
    for (let index = 0; index < dataObject.length; index++) {
        const match = dataObject[index];
        const matchYear = match.season;
        const id = match.id;
        result[id] = matchYear;
    }
    // console.log(result);
    return result;
}

module.exports = getEachYearMatchId;
